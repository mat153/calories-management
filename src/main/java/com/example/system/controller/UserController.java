package com.example.system.controller;


import com.example.system.model.Users;
import com.example.system.repository.UserRepository;
import com.example.system.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;


@Controller
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    private final UserRepository userRepository;

    @GetMapping("/sign-up")
    public String signUp(Model model){
        model.addAttribute("user",new Users());
        return "sign-up";
    }

    @PostMapping("/register")
    public String register(Users user,Model model) throws Exception {
        if (userService.checkUserInDatabase(user)) {
            userService.addUser(user);
            return "login";
        }
        else {
            model.addAttribute("errorMessage","Register failed");
        return "sign-up";
        }
    }


}

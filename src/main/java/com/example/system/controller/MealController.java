package com.example.system.controller;

import com.example.system.model.*;
import com.example.system.repository.MealRepository;
import com.example.system.service.MealService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.text.DecimalFormat;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class MealController {

    private final MealRepository mealRepository;
    private final MealService mealService;
    private final SingleItemCaloriesApi singleItemCaloriesApi;
    private static DecimalFormat decimalFormat = new DecimalFormat("0.00");


    @GetMapping("/all")
    public String showAllMeals(Model model){
        //model.addAttribute("meals",mealRepository.findAll());
        return findPaginated(1,model);
        //return "all_meals";
    }

    @GetMapping("/newMeal")
    public String showNewMealForm(Model model){
        Meal meal = new Meal();
        model.addAttribute("meal",meal);
        return "new_meal";
    }

    @PostMapping("/add-meal")
    public String saveMeal(@Valid Meal meal, BindingResult result, Model model,Principal principal)throws IOException {

        DateLogged dateLogged = new DateLogged();
        meal.setDate(dateLogged.toDateToday());
        meal.setUserId(principal.getName());
        SingleItemCaloriesApi singleItemCaloriesApi = new SingleItemCaloriesApi();
        meal.setCalories(singleItemCaloriesApi.getCaloriesSingleMeal(dateLogged.toDateToday(), principal.getName(),meal));
        mealRepository.save(meal);
        return "redirect:/";
    }

    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        Meal meal = mealRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid meal Id:" + id));

        model.addAttribute("meal", meal);
        return "update-meal";
    }

    @PostMapping("/update/{id}")
    public String updateMeal(@PathVariable("id") long id, @Valid Meal meal,
                             BindingResult result, Model model, Principal principal) {
        if (result.hasErrors()) {
            meal.setId(id);
            return "update-meal";
        }

        String date = mealRepository.getOne(id).getDate();

        meal.setDate(date);
        meal.setUserId(principal.getName());

        mealRepository.save(meal);
        return "redirect:/all";
    }

    @GetMapping("/delete/{id}")
    public String deleteMeal(@PathVariable("id") long id, Model model) {
        Meal meal = mealRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        mealRepository.delete(meal);
        return "redirect:/all";
    }

    //bez zewnetrznego api tylko z naszej bazy
    @GetMapping("/dayCalories")
    public String showTodayCalories(Model model,Principal principal)
    {
        DateLogged dateLogged = new DateLogged();
        List<Meal> lists = mealService.getMealsDay(dateLogged.toDateToday(),principal.getName());
        double allCaloriesToday = 0.0;
        for (Meal meal: lists){
            allCaloriesToday = allCaloriesToday + meal.getCalories();
        }

        String allCaloriesString = decimalFormat.format(allCaloriesToday);

        //String allCaloriesString = Double.toString(allCaloriesToday);
        model.addAttribute("allCalories",allCaloriesString);

        return "day-calories";
    }

    @GetMapping("/yesterdayCalories")
    public String showYesterdayCalories(Model model,Principal principal)
    {
        DateLogged dateLogged = new DateLogged();
        List<Meal> lists = mealService.getMealsDay(dateLogged.toDateYesterday(),principal.getName());
        double allCaloriesToday = 0.0;
        for (Meal meal: lists){
            allCaloriesToday = allCaloriesToday + meal.getCalories();
        }
        String allCaloriesString = decimalFormat.format(allCaloriesToday);

        model.addAttribute("allCalories",allCaloriesString);

        return "yesterday-calories";
    }

    @GetMapping("/todayDetailsResults")
    public String showDetailsResults(Model model,Principal principal) throws IOException {

        DateLogged dateLogged = new DateLogged();
        String strDate = dateLogged.toDateToday();

        CaloriesApi secondCaloriesApi = new CaloriesApi(mealRepository, singleItemCaloriesApi);
        List<Item> items = secondCaloriesApi.calories(strDate,principal.getName());

        List<Result> resultDetails = secondCaloriesApi.getDetailedResults(items);

        model.addAttribute("mealsDetails",resultDetails);
        return "today-detailsResult";
    }

    @GetMapping("/todayCaloriesResults")
    public String showCaloriesResults(Model model,Principal principal) throws IOException {

        DateLogged dateLogged = new DateLogged();
        String strDate = dateLogged.toDateToday();

        CaloriesApi secondCaloriesApi = new CaloriesApi(mealRepository, singleItemCaloriesApi);
        List<Item> items = secondCaloriesApi.calories(strDate,principal.getName());
        List<Result> resultCalories = secondCaloriesApi.getSimpleResults(items);

        model.addAttribute("mealsCalories",resultCalories);
        return "today-caloriesResult";
    }

    @GetMapping("/dayMeals")
    public String showDayMeals(Model model, Principal principal) throws IOException {

        DateLogged dateLogged = new DateLogged();
        String strDate = dateLogged.toDateToday();
/*        UserAuth userAuth = new UserAuth();
        String currentPrincipalName= userAuth.getCurrentPrincipalName();*/
        List <Meal> meals = mealService.getMealsDay(strDate,principal.getName());
/*
        List<Meal> meals = mealRepository.findByDateAndUserId(strDate,currentPrincipalName);
*/
        model.addAttribute("meals",meals);
        return "day-meals";
    }

    @GetMapping("/yesterdayMeals")
    public String showYesterdayMeals(Model model,Principal principal) throws IOException {

        DateLogged dateLogged = new DateLogged();
        String strDate = dateLogged.toDateYesterday();

        List<Meal> meals = mealService.getMealsDay(strDate,principal.getName());
        model.addAttribute("meals",meals);
        return "yesterday-meals";
    }

    @GetMapping("/allMeals")
    public String showAllYourMeals(Model model,Principal principal) throws IOException{

        List<Meal> yourMeals = mealRepository.findByUserId(principal.getName());
        model.addAttribute("yourMeals",yourMeals);

        return "all-userMeals";
    }

    @GetMapping("/allWeek")
        public String showWeek(Model model, Principal principal) {
        DateLogged dateLogged = new DateLogged();
        List<String> var = dateLogged.allWeekDay();
        double allCaloriesWeek = mealService.getMealsCertainTime(var,principal);

        String allWeekCaloriesString = decimalFormat.format(allCaloriesWeek);

        model.addAttribute("allCalories",allWeekCaloriesString);
        return "all-week-calories";
    }

    @GetMapping("/allMonth")
    //@ResponseBody kiedy nie rzucamy do thymeleaf tylko chce wyswietlic zwyklego stringa i zwracamy stringa
    public String showMonth(Model model,Principal principal){
        DateLogged dateLogged = new DateLogged();
        List<String> var = dateLogged.allMonthDay();
        double allMonthCalories = mealService.getMealsCertainTime(var,principal);
        String allMonthCaloriesString = decimalFormat.format(allMonthCalories);
        model.addAttribute("allCalories",allMonthCaloriesString);

        return "all-month-calories";
    }

    @GetMapping("/page/{pageNo}")
    public String findPaginated(@PathVariable (value = "pageNo") int pageNo,Model model){
        int pageSize = 10;
        Page<Meal> page = mealService.findPaginated(pageNo,pageSize);
        List<Meal> mealList = page.getContent();
        model.addAttribute("currentPage",pageNo);
        model.addAttribute("totalPages",page.getTotalPages());
        model.addAttribute("totalItems",page.getTotalElements());
        model.addAttribute("meals",mealList);

        return "all_meals";
    }


}

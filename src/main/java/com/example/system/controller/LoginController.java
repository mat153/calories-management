package com.example.system.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

@Controller
public class LoginController {

    @GetMapping("/login")
    public String login(){return "login";}

    @GetMapping("/")
    public String home(Principal principal) {
        if (principal.getName().equals("admin")){
            return "admin-site";
        }
        else return "index";
    }

}

package com.example.system.model;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class Item__1 {

    @SerializedName("sugar_g")
    @Expose
    private Double sugarG;
    @SerializedName("fiber_g")
    @Expose
    private Double fiberG;
    @SerializedName("serving_size_g")
    @Expose
    private Double servingSizeG;
    @SerializedName("sodium_mg")
    @Expose
    private Integer sodiumMg;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("potassium_mg")
    @Expose
    private Integer potassiumMg;
    @SerializedName("fat_saturated_g")
    @Expose
    private Double fatSaturatedG;
    @SerializedName("fat_total_g")
    @Expose
    private Double fatTotalG;
    @SerializedName("calories")
    @Expose
    private Double calories;
    @SerializedName("cholesterol_mg")
    @Expose
    private Integer cholesterolMg;
    @SerializedName("protein_g")
    @Expose
    private Double proteinG;
    @SerializedName("carbohydrates_total_g")
    @Expose
    private Double carbohydratesTotalG;

    public Double getSugarG() {
        return sugarG;
    }

    public void setSugarG(Double sugarG) {
        this.sugarG = sugarG;
    }

    public Double getFiberG() {
        return fiberG;
    }

    public void setFiberG(Double fiberG) {
        this.fiberG = fiberG;
    }

    public Double getServingSizeG() {
        return servingSizeG;
    }

    public void setServingSizeG(Double servingSizeG) {
        this.servingSizeG = servingSizeG;
    }

    public Integer getSodiumMg() {
        return sodiumMg;
    }

    public void setSodiumMg(Integer sodiumMg) {
        this.sodiumMg = sodiumMg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPotassiumMg() {
        return potassiumMg;
    }

    public void setPotassiumMg(Integer potassiumMg) {
        this.potassiumMg = potassiumMg;
    }

    public Double getFatSaturatedG() {
        return fatSaturatedG;
    }

    public void setFatSaturatedG(Double fatSaturatedG) {
        this.fatSaturatedG = fatSaturatedG;
    }

    public Double getFatTotalG() {
        return fatTotalG;
    }

    public void setFatTotalG(Double fatTotalG) {
        this.fatTotalG = fatTotalG;
    }

    public Double getCalories() {
        return calories;
    }

    public void setCalories(Double calories) {
        this.calories = calories;
    }

    public Integer getCholesterolMg() {
        return cholesterolMg;
    }

    public void setCholesterolMg(Integer cholesterolMg) {
        this.cholesterolMg = cholesterolMg;
    }

    public Double getProteinG() {
        return proteinG;
    }

    public void setProteinG(Double proteinG) {
        this.proteinG = proteinG;
    }

    public Double getCarbohydratesTotalG() {
        return carbohydratesTotalG;
    }

    public void setCarbohydratesTotalG(Double carbohydratesTotalG) {
        this.carbohydratesTotalG = carbohydratesTotalG;
    }

    @Override
    public String toString() {
        return "Item__1{" +
                "sugarG=" + sugarG +
                ", fiberG=" + fiberG +
                ", servingSizeG=" + servingSizeG +
                ", sodiumMg=" + sodiumMg +
                ", name='" + name + '\'' +
                ", potassiumMg=" + potassiumMg +
                ", fatSaturatedG=" + fatSaturatedG +
                ", fatTotalG=" + fatTotalG +
                ", calories=" + calories +
                ", cholesterolMg=" + cholesterolMg +
                ", proteinG=" + proteinG +
                ", carbohydratesTotalG=" + carbohydratesTotalG +
                '}';
    }
}

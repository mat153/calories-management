package com.example.system.model;

public class Result {

    private final String name;
    private final Double calories;
    private final Integer sodium;
    private final Double fiber;


    public Double getCalories() {
        return calories;
    }

    public Integer getSodium() {
        return sodium;
    }

    public Double getFiber() {
        return fiber;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return
                "name='" + name + '\'' +
                ", calories=" + calories +
                ", sodium=" + sodium +
                ", fiber=" + fiber
                ;
    }

    private Result(ResultBuilder builder) {
        this.calories = builder.calories;
        this.fiber = builder.fiber;
        this.sodium = builder.sodium;
        this.name = builder.name;
    }

    public static class ResultBuilder {
        private final Double calories;
        private Integer sodium;
        private Double fiber;
        private String name;

        public ResultBuilder(Item item) {
            this.calories = item.getItems().get(0).getCalories();
        }

        public ResultBuilder withName(Item item) {
            this.name = item.getItems().get(0).getName();
            return this;
        }

        public ResultBuilder withFiber(Item item) {
            this.fiber = item.getItems().get(0).getFiberG();
            return this;
        }

        public ResultBuilder withSodium (Item item) {
            this.sodium = item.getItems().get(0).getSodiumMg();
            return this;
        }

        public Result build () {

            Result result = new Result(this);

            return result;
        }

    }
}


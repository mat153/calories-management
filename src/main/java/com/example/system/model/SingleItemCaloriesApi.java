package com.example.system.model;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;


@Service
public class SingleItemCaloriesApi {

    public Double getCaloriesSingleMeal(String strDate, String currentPrincipalName,Meal meal) throws IOException {

        String what = meal.getName();
        String howMuch = meal.getAmount().toString();
        String whatRegex = what.replaceAll("\\s", "%20"); //zastapienie spacji %20
        Item item = downloadApi(what,howMuch,whatRegex);
        return item.getItems().get(0).getCalories();

    }

    public Item downloadApi(String what, String howMuch, String whatRegex) throws IOException {

        //pobranie api
        String word = howMuch + "%20" + "grams" + "%20" + whatRegex;
        URL url = new URL("https://api.calorieninjas.com/v1/nutrition?query=" + word);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.addRequestProperty("X-Api-Key", "ZQ7wXe3SVr6OhST0cYJRWg==18L6ETPaOkOIQL73");

        InputStream response = connection.getInputStream();
        Scanner scanner = new Scanner(response);
        String responseBody = scanner.useDelimiter("\\A").next();

        JsonObject jsonObject = JsonParser.parseString(responseBody).getAsJsonObject();

        Item item = new Gson().fromJson(jsonObject, Item.class);
        return item;
    }
}

package com.example.system.model;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

public class DateLogged {
    private Locale locale = Locale.UK;

    public String toDateToday() {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 0);
        Date today = calendar.getTime();

        return simpleDateFormat.format(today).toString();
    }

    public String toDateYesterday() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Calendar calendar = Calendar.getInstance();
        // Move calendar to yesterday
        calendar.add(Calendar.DATE, -1);
        Date yesterday = calendar.getTime();

        return simpleDateFormat.format(yesterday).toString();
    }

    public List<String> allWeekDay(){

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.set(Calendar.DAY_OF_WEEK,calendar.getFirstDayOfWeek());

        List<String> dayWeek = new ArrayList<>();

        for (int i = 0; i < 7; i++) {
            String dayThisWeek = simpleDateFormat.format(calendar.getTime()).toString();
            // 5. increase day field;
            dayWeek.add(dayThisWeek);
            calendar.add(Calendar.DAY_OF_WEEK, 1);
        }
        return dayWeek;
    }

    public List<String> allMonthDay() {
        List<String> dayMonth = new ArrayList<>();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        int lastDayOfMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);


        for (int i = 0;i<lastDayOfMonth;i++)
        {
            String dayThisMonth = simpleDateFormat.format(calendar.getTime()).toString();
            dayMonth.add(dayThisMonth);
            calendar.add(Calendar.DAY_OF_MONTH,1);
        }

        return dayMonth;
    }




    }

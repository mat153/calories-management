package com.example.system.model;

import com.example.system.repository.MealRepository;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CaloriesApi {

    private final MealRepository mealRepository;
    private final  SingleItemCaloriesApi singleItemCaloriesApi;

    public CaloriesApi(MealRepository mealRepository, SingleItemCaloriesApi singleItemCaloriesApi) {
        this.mealRepository = mealRepository;
        this.singleItemCaloriesApi = singleItemCaloriesApi;
    }

    public List<Item> calories(String strDate, String currentPrincipalName) throws IOException {

        List<Meal> meals = mealRepository.findByDateAndUserId(strDate, currentPrincipalName);
        List<Item> items = new ArrayList<>();

        for (Meal meal : meals) {
            String what = meal.getName();
            String howMuch = meal.getAmount().toString();
            String whatRegex = what.replaceAll("\\s", "%20"); //zastapienie spacji %20

            //pobranie api
            String word = howMuch + "%20" + "grams" + "%20" + whatRegex;
            URL url = new URL("https://api.calorieninjas.com/v1/nutrition?query=" + word);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.addRequestProperty("X-Api-Key", "ZQ7wXe3SVr6OhST0cYJRWg==18L6ETPaOkOIQL73");

            InputStream response = connection.getInputStream();
            Scanner scanner = new Scanner(response);
            String responseBody = scanner.useDelimiter("\\A").next();

            JsonObject jsonObject = JsonParser.parseString(responseBody).getAsJsonObject();
            items.add(new Gson().fromJson(jsonObject, Item.class));
        }
        return items;
    }

    public List<Result> getSimpleResults(List<Item> items){
        List<Result> results = new ArrayList<>();
        for (Item item : items){
            results.add(new Result.ResultBuilder(item).withName(item).build());
        }
        return results;
    }

    public List<Result> getDetailedResults(List<Item> items){
        List<Result> results = new ArrayList<>();
        for (Item item : items){
            results.add(new Result.ResultBuilder(item).withFiber(item).withSodium(item).withName(item).build());
        }
        return results;
    }

    public String allCalories(String strDate, String currentPrincipalName) throws IOException {

        List<Meal> meals = mealRepository.findByDateAndUserId(strDate, currentPrincipalName);

        double allCalories = 0.0;

        for (Meal meal : meals) {
            String what = meal.getName();
            String howMuch = meal.getAmount().toString();
            String whatRegex = what.replaceAll("\\s", "%20"); //zastapienie spacji %20

            Item item = singleItemCaloriesApi.downloadApi(what,howMuch,whatRegex);

            Double presentCalories = item.getItems().get(0).getCalories();

            allCalories = allCalories + presentCalories;

        }

        return Double.toString(allCalories);
    }



}


package com.example.system.model;


import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@RequiredArgsConstructor
@Getter
@Setter
public class Meal {

    private @Id @GeneratedValue Long id;
    private String name;
    private Integer amount;
    private String userId;
    private String date;
    private Double calories;

    @Override
    public String toString() {
        return "Meal{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", amount=" + amount +
                ", userId='" + userId + '\'' +
                ", date='" + date + '\'' +
                ", calories=" + calories +
                '}';
    }
}

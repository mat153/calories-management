package com.example.system.service;

import com.example.system.model.Meal;
import com.example.system.repository.MealRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;

@Service
public class MealService {
    private final MealRepository mealRepository;

    public MealService(MealRepository mealRepository) {
        this.mealRepository = mealRepository;
    }


    public List<Meal> getMealsDay(String date,String userId) {
        return mealRepository.findByDateAndUserId(date,userId);
    }


    public double getMealsCertainTime(List<String> var, Principal principal) {

        double allCalories = 0.0;
        for (String x : var) {
            List<Meal> lists = getMealsDay(x, principal.getName());
            double allCaloriesDay = 0.0;
            for (Meal meal : lists) {
                allCaloriesDay = allCaloriesDay + meal.getCalories();
            }
            allCalories = allCalories + allCaloriesDay;
        }

        return allCalories;
    }

    public Page<Meal> findPaginated(int No, int pageSize){

        Pageable pageable = PageRequest.of(No-1,pageSize);
        return this.mealRepository.findAll(pageable);
    }

}

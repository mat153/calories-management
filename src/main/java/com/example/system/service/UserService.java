package com.example.system.service;

import com.example.system.model.Users;
import com.example.system.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public void addUser(Users user) throws Exception {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            user.setRole("ROLE_USER");
            userRepository.save(user);
    }

    public boolean checkUserInDatabase(Users user) {
        List<Users> users = userRepository.findAll();
        for (Users x : users) {
            if (x.getUsername().equals(user.getUsername()))
                return false;
        }
        return true;
    }

}

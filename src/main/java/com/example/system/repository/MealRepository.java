package com.example.system.repository;

import com.example.system.model.Meal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MealRepository extends JpaRepository<Meal,Long> {
    List<Meal> findByDateAndUserId(String date,String userId);
    List<Meal> findByUserId(String username);
}
